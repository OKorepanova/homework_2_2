var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];

console.log ('Задание 1');
for (var i=0; i<studentsAndPoints.length; i=i+2){
  console.log ('Студент '+studentsAndPoints[i]+' набрал '+studentsAndPoints[i+1]+' баллов');
}
console.log ('');
console.log ('Задание 2');
var maxPoints=-1;
var maxIndex=-1;
for (var i=0; i<studentsAndPoints.length; i=i+2){
  if (studentsAndPoints[i+1] >= maxPoints) {
      maxPoints=studentsAndPoints[i+1];
      maxIndex=i;
      }
}
if (maxIndex>=0){
 console.log ('Студент набравший максимальный балл:');
 console.log ('Студент '+studentsAndPoints[maxIndex]+' имеет максимальный балл: '+studentsAndPoints[maxIndex+1]+' баллов');
}
  else {
    console.log ('Исходные данные некорректны')
  }

console.log ('');
console.log ('Задание 3');
studentsAndPoints.push ('Николай Фролов', 0);
studentsAndPoints.push ('Олег Боровой', 0);
for (var i=0; i<studentsAndPoints.length; i=i+2){
  console.log ('Студент '+studentsAndPoints[i]+' набрал '+studentsAndPoints[i+1]+' баллов');
}

console.log ('');
console.log ('Задание 4');
var indexAdd;

indexAdd=studentsAndPoints.indexOf('Антон Павлович');
if (indexAdd>=0){
  studentsAndPoints[indexAdd+1]+=10;
}
else {
  console.log ('Такого студента в списке нет');
}

indexAdd=studentsAndPoints.indexOf('Николай Фролов');
if (indexAdd>=0){
  studentsAndPoints[indexAdd+1]+=10;
}
else {
  console.log ('Такого студента в списке нет');
}
  
for (var i=0; i<studentsAndPoints.length; i=i+2){
  console.log ('Студент '+studentsAndPoints[i]+' набрал '+studentsAndPoints[i+1]+' баллов');
}

console.log ('');
console.log ('Задание 5');
console.log ('Студенты не набравшие баллов:')
for (var i=0; i<studentsAndPoints.length; i=i+2){
  if (studentsAndPoints[i+1]==0) {
  console.log (studentsAndPoints[i]);
  }
}

console.log ('');
console.log ('Задание 6');

for (var i=studentsAndPoints.length; i>=0; i=i-2){
  if (studentsAndPoints[i-1]==0) {
  studentsAndPoints.splice(i-2,2);
  }
}

for (var i=0; i<studentsAndPoints.length; i=i+2){
  console.log ('Студент '+studentsAndPoints[i]+' набрал '+studentsAndPoints[i+1]+' баллов');
}
